//
//  HeroAnimationTableApp.swift
//  HeroAnimationTable
//
//  Created by Eduard Kanevskii on 25.08.2023.
//

import SwiftUI

@main
struct HeroAnimationTableApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
